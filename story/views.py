from django.shortcuts import render
from . forms import formi

# Create your views here.
def index (request):
    return render(request, 'story/index.html')

def contact (request):
    return render(request, 'story/contact.html')

def friendform(request):
    if request.method == 'POST':
        form = formi(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            return render(request, 'story/thanks.html')
    else:
        form = formi()
        return render(request, 'story/getintouch.html', {'form' : form})

def thanks(request):
    return render(request, 'story/thanks.html')
